Standard git-rebase workflow              | unlike any
Delta queue editing is immediate and easy |  other tool

May always git-cherry-pick from upstream  | unlike any other
May always git commit -m fixup!           |  delta queue
May make "mixed" commits                  |   workflow

No in-tree metadata                       | unlike git-dpm

============================================================

No need to switch branches                | unlike gbp pq and git-dpm
Tree always dpkg-buildpackage'able        | unlike gbp pq and git-dpm

Working tree is never dirtied
 - by patch application                   | unlike gbp pq
 - by metadata conflicts
 - by patch export                        | unlike gbp pq

No reading diffs of diffs                 | unlike gbp pq

============================================================

git-blame (and git-log file) work         | like dgit-maint-merge
Tree makes sense to upstreams & users     |  and git-dpm

No need to ever use quilt(1)
Can mostly ignore `3.0 (quilt)' .dsc
`3.0 (quilt)' .dsc output is always perfect

Directly compatible with dgit.
No command needed to upload other than dgit push.
