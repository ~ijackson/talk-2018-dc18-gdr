
SLIDES+= context
SLIDES+= usp-1
SLIDES+= usp-2
SLIDES+= usp-3
SLIDES+= dm-1
SLIDES+= dm-2
SLIDES+= dm-rebasing
SLIDES+= dm-3
SLIDES+= dm-pre-newups
SLIDES+= dm-5
SLIDES+= dm-6
SLIDES+= dm-zoom # made from 6
SLIDES+= dm-6
SLIDES+= dm-rebase-again # made from 7
SLIDES+= dm-pre-wreck
SLIDES+= dm-wreck # made from 4
SLIDES+= dm-post-wreck
SLIDES+= dm-6
SLIDES+= refs
# dm-for-zoom is made from 6
#SLIDES+= manpage

# layers
#   1X0    text
#   1X1    circles
#   1X2    edges
#   1X3    edges (53 is included in wreck)
#   1X4,5  upstream git history
#   1X6    include in zooms
#   2X0-9  "master" note
#   2X3    not included in pre-wreck
#   251    included in rebase-again
#[12]X6    exclude from zoom
# for
#    X =
#      1      [1]
#      2      [2]
#      3      [3]
#      5      [4]
#      6      [5]


SLIDEFILES=$(addsuffix .ps, $(SLIDES))

o= >$@.new && mv -f $@.new $@

all:	slides.pdf talk.ps demo.ps

%.ps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4  <$@.1 $o

zoom.fig: dm-for-zoom.eps

dm-%.ps: dm.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+110:1$*9,2$*0:2$*9 <$@.1 $o

dm-rebasing.ps: dm.fig helm-rebase-crop.ppm
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+24,110:129,220:229 <$@.1 $o

dm-rebase-again.ps: dm.fig conflict.txt.eps
	./greyout <$< >$@.fig.tmp
	iconv <$@.fig.tmp >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+110:179,251,270:279 <$@.1 $o

dm-zoom.ps: dm.fig conflict.txt.eps
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+10:11,110:165,167:169,260:265,266:269 <$@.1 $o

dm-pre-wreck.ps: dm.fig conflict.txt.eps
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+110:139,144:145,230:232,235:239 <$@.1 $o

dm-post-wreck.ps: dm.fig conflict.txt.eps
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+4,5,110:139,144:145,230:232,235:239 <$@.1 $o

dm-pre-newups.ps: dm.fig conflict.txt.eps
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+110:139,144:145,230:232,235:239 <$@.1 $o

dm-wreck.ps: dm.fig conflict.txt.eps
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+1:3,5,144:145,153,+110:149,240:249 <$@.1 $o

helm-rebase-crop.ppm: helm-rebase.png Makefile
	pngtopnm <$< | pnmcut -right 550 -bottom 300 >$@

%.eps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L eps <$@.1 $o

dm-for-zoom.eps: dm.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L eps -D+110:118,120:169,260:268  <$@.1 $o

%.txt.eps: %.txt ./txt2ps
	./txt2ps <$< |ps2eps -s a3 $o

%.ps:	%.lout
	lout $< $o

slides.ps: $(SLIDEFILES) Makefile
	cat $(SLIDEFILES) $o

%.pdf:     %.ps Makefile
	ps2pdf $< $@

talk.ps demo.ps: %.ps: %.txt Makefile
	a2ps -1 -o $@.1.tmp -B $<
	pstops <$@.1.tmp >$@ '0@0.94(7mm,7.5mm)'

usb=/media/usb1

for-printing: talk.pdf demo.pdf
	mount $(usb)
	cp $^ $(usb)/.
	umount $(usb)

install: slides.pdf talk.txt
	rsync -vP $^ ijackson@chiark:public-html/2018/debconf-gdr-talk/
	git push ijackson@chiark:public-git/talk-2018-dc18-gdr.git
